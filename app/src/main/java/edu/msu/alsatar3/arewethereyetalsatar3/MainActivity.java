package edu.msu.alsatar3.arewethereyetalsatar3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.prefs.PreferenceChangeEvent;


public class MainActivity extends AppCompatActivity {
    private LocationManager locationManager = null;
    private final static String P = "pre";
    private SharedPreferences settings;
    private double latitude = 0;
    private double longitude = 0;
    private boolean valid = false;
    private final static String TO = "to";
    private final static String TOLAT = "tolat";
    private final static String TOLONG = "tolong";
    private final static String tran = "tran";
    private ActiveListener activeListener = new ActiveListener();
    private double toLatitude = 0;
    private double toLongitude = 0;
    private String to = "";
    private final String[] choices = {"Driving", "Walking", "Bicycling"};
    private int trans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        settings= PreferenceManager.getDefaultSharedPreferences(this);
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Also, dont forget to add overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //   int[] grantResults)
                // to handle the case where the user grants the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
        // Get the location manager
        // Force the screen to say on and bright
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        to = settings.getString(TO, "2250 Engineering");
        TextView viewloct = (TextView)findViewById(R.id.n1);
        viewloct.setText(to);
        float orgx = (float)(42.724303);
        float orgy = (float)(-84.480507);
        toLatitude = (double)(settings.getFloat(TOLAT, orgx));
        toLongitude = (double)(settings.getFloat(TOLONG, orgy));
        trans = settings.getInt(tran, 0);
        TextView trant = (TextView)findViewById(R.id.trans);
        trant.setText(choices[trans]);
    }
    @Override
    protected void onResume() {
        super.onResume();
        TextView viewProvider = (TextView)findViewById(R.id.provs);
        viewProvider.setText("");
        setUI();
        registerListeners();
    }
    @Override
    protected void onPause() {
        unregisterListeners();
        super.onPause();
    }
    private void setUI() {
        TextView viewLatitude = (TextView)findViewById(R.id.lats);
        TextView viewLongitude = (TextView)findViewById(R.id.lns);
        TextView viewDistance = (TextView)findViewById(R.id.n2);
        TextView address = (TextView)findViewById(R.id.n1);
        if(valid){
            viewLatitude.setText(String.valueOf(toLatitude));
            viewLongitude.setText(String.valueOf(toLongitude));
            address.setText(to);
            float[] distance = new float[1];
            Location.distanceBetween(latitude,longitude,toLatitude,toLongitude,distance);
            viewDistance.setText(String.format("%1$6.1fm", distance[0]));
        }
        else{
            viewLatitude.setText("");
            viewLongitude.setText("");
            viewDistance.setText("");
            address.setText("");
        }
    }
    private class ActiveListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            onLocation(location);
        }

        @Override
        public void onStatusChanged(String s, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {
            registerListeners();
        }
    };
    private void registerListeners() {
        unregisterListeners();

        // Create a Criteria object
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setAltitudeRequired(true);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(false);

        String bestAvailable = locationManager.getBestProvider(criteria, true);

        if(bestAvailable != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(bestAvailable, 500, 1, activeListener);
            TextView viewProvider = (TextView)findViewById(R.id.provs);
            viewProvider.setText(bestAvailable);
            Location location = locationManager.getLastKnownLocation(bestAvailable);
            onLocation(location);
        }
    }
    private void onLocation(Location location) {
        if(location == null) {
            return;
        }

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        valid = true;

        setUI();
    }
    private void unregisterListeners() {
        locationManager.removeUpdates(activeListener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    /**
     * Handle setting a new "to" location.
     * @param address Address to display
     * @param lat latitude
     * @param lon longitude
     */
    private void newTo(String address, double lat, double lon) {
        to = address;
        toLatitude = lat;
        toLongitude = lon;
        SharedPreferences.Editor myEdit = settings.edit();
        myEdit.putString(TO,to);
        myEdit.putFloat(TOLAT,(float)(toLatitude));
        myEdit.putFloat(TOLONG,(float)(toLongitude));
        myEdit.commit();
        setUI();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.itemSparty:
                newTo("Sparty", 42.731138, -84.487508);
                return true;

            case R.id.itemHome:
                newTo("Home", 39.36025324, -76.40443923);
                return true;

            case R.id.item2250:
                newTo("2250 Engineering", 42.724303, -84.480507);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onNew(View view) {
        EditText location = (EditText)findViewById(R.id.input);
        final String address = location.getText().toString().trim();
        newAddress(address);
    }

    private void newAddress(final String address) {
        if(address.equals("")) {
            // Don't do anything if the address is blank
            return;
        }
        final Thread x =new Thread(new Runnable() {
            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable(){
                    public void run()
                    {
                        lookupAddress(address);
                    }
                });
            }

        });
        x.start();
    }
    private void lookupAddress(final String address) {
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.US);
        boolean exception;
        exception = false;
        List<Address> locations;
        try {
            locations = geocoder.getFromLocationName(address, 1);
        } catch(IOException ex) {
            // Failed due to I/O exception
            locations = null;
            exception = true;
        }
        newLocation( address, exception, locations);
    }
    private void newLocation(String address, boolean exception, List<Address> locations) {

        if(exception) {
            Toast.makeText(MainActivity.this, R.string.exception, Toast.LENGTH_SHORT).show();
        } else {
            if(locations == null || locations.size() == 0) {
                Toast.makeText(MainActivity.this, R.string.couldnotfind, Toast.LENGTH_SHORT).show();
                return;
            }

            EditText location = (EditText)findViewById(R.id.input);
            location.setText("");

            // We have a valid new location

            Address a = locations.get(0);
            newTo(address, a.getLatitude(), a.getLongitude());

        }
    }
    public void transportation(View view){
        final String[] choices = {"Driving", "Walking", "Bicycling"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select a type of transportation:");
        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                trans= which;
                TextView trant = (TextView)findViewById(R.id.trans);
                trant.setText(choices[trans]);
                SharedPreferences.Editor myEdit = settings.edit();
                myEdit.putInt(tran,trans);
                myEdit.commit();
            }
        });
        builder.show();
    }
    public void display(View view){
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}
